extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _input(event):
	if event.is_action_released("ui_accept"):
		OS.set_window_fullscreen(!OS.is_window_fullscreen())
	elif event.is_action_released("ui_cancel"):
		get_tree().quit()
