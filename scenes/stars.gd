extends Label


export(int) var speed = 1

var line1 = "        *      .     *                .             .        "
var line2 = "          .                                   .             *       .     "
var line3 = "      .            .                  *                               *    "
var line4 = "                  *        .         *              .    .            "

var register1 = "                   .           *     .  "
var register2 = "   *                              .     "
var register3 = "              *                 *       "
var register4 = "            .           .              ."

func update():
	var one = scroll(line1, register1, speed)
	line1 = one[0]
	register1 = one[1]

	var two = scroll(line2, register2, speed)
	line2 = two[0]
	register2 = two[1]

	var three = scroll(line3, register3, speed)
	line3 = three[0]
	register3 = three[1]

	var four = scroll(line4, register4, speed)
	line4 = four[0]
	register4 = four[1]

	text = register1 + "\n" + register2 + "\n" + register3 + "\n" + register4

func scroll(line, register, speed):
	for i in range(speed):
		var temp = register
		register = register.substr(1) + line.substr(0, 1)
		line = line.substr(1) + temp.substr(0, 1)
	return [line, register]

func _on_Timer_timeout():
	update()
