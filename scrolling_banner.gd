extends Label

const SPACES = "                                                                                "
const DEFAULT_TEXT_PATH = "res://bottom_text.txt"
const USER_TEXT_PATH = "user://bottom_text.txt"

func _ready():
	var bottom_text_file = File.new()
	if bottom_text_file.file_exists(USER_TEXT_PATH):
		bottom_text_file.open(USER_TEXT_PATH, File.READ)
	else:
		bottom_text_file.open(DEFAULT_TEXT_PATH, File.READ)

	var bottom_text = bottom_text_file.get_as_text()

	var user_text_file = File.new()
	user_text_file.open(USER_TEXT_PATH, File.WRITE)
	user_text_file.store_string(bottom_text)
	user_text_file.close()

	text = bottom_text.strip_edges() + " "

func scroll():
	text = text.substr(1) + text.substr(0, 1)
	var new_color = get("custom_colors/font_color")
	new_color.h = (new_color.h + (3.0 / 360.0))
	if new_color.h > 1.0:
		new_color.h -= 1.0

	set("custom_colors/font_color", new_color)

func _on_Timer_timeout():
	scroll()
